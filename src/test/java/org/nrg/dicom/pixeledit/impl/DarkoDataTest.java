package org.nrg.dicom.pixeledit.impl;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class DarkoDataTest {
    @Test
    @Disabled("Loads DICOM files from local folders")
    public void process() {
        try {
            Map<String, String> testdata = new HashMap<>();
//            testdata.put("/Users/drm/projects/nrg/blankPixelData/from-darko/sample2-slim/1/1.2.840.113654.2.45.2.108105-1-1-2z65o7.dcm", "/tmp/pe.dcm");
            testdata.put("/Users/drm/Downloads/sample2-slim/1/1.2.840.113654.2.45.2.108105-1-1-2z65o7.dcm", "/tmp/pe.dcm");
//            testdata.put("/Users/drm/Downloads/sample2-slim/1/1.2.840.113654.2.45.2.108105-1-5-2ypk0a.dcm", "/tmp/pe.dcm");

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for (String srcString : testdata.keySet()) {
                File                     src     = new File(srcString);
                File                     dst     = new File(testdata.get(srcString));
                DicomObjectI             dobj    = DicomObjectFactory.newInstance(src);
                Rectangle2D              rect    = new Rectangle2D.Float(50, 50, 100, 50);
                PixelmedPixelEditHandler handler = new PixelmedPixelEditHandler();
                Color                    c       = new Color(128, 128, 128);
                handler.process(rect, c, dobj);
                if (!dst.getParentFile().exists()) {
                    Files.createDirectories(dst.getParentFile().toPath());
                }
                dobj.write(new FileOutputStream(dst));
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }
}