package org.nrg.dicom.pixeledit.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.fail;

public class CtractUSDataTest {

    @Test
    @Disabled
    public void process() {
        try {
            Map<String, String> testdata = new HashMap<>();
            testdata.put("/Users/drm/Box/CTRACT_Data/Burned_in_PHI/Northwestern_TestCase1_US1/SCANS/1-Venous/DICOM/Northwestern_TestCase1.US.CTRACT.1.16.20180515.093201.125sgoq.dcm", "/tmp/us.dcm");
            testdata.put("/Users/drm/Box/CTRACT_Data/Burned_in_PHI/Northwestern_TestCase1_US1/SCANS/1-Venous/DICOM/Northwestern_TestCase1.US.CTRACT.1.8.20180515.093201.ijn36n.dcm", "/tmp/us_multi.dcm");

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( String srcString: testdata.keySet()) {
                File src = new File( srcString);
                File dst = new File(testdata.get(srcString));
                DicomObjectI dobj = DicomObjectFactory.newInstance( src);
                Rectangle2D rect = new Rectangle2D.Float(50, 50, 200, 150);
                PixelmedPixelEditHandler handler = new PixelmedPixelEditHandler();
                Color c = new Color(128, 128, 128);
                handler.process(rect, c, dobj);
                if( ! dst.getParentFile().exists()) Files.createDirectories( dst.getParentFile().toPath());
                dobj.write( new FileOutputStream( dst));
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }
}