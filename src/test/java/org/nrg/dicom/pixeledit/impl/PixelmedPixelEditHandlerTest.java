package org.nrg.dicom.pixeledit.impl;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PixelmedPixelEditHandlerTest {

    @Test
    void process() {
        try {
            List<Data> dataList = new ArrayList<>();
            // passes
            dataList.add( new Data("single-frame/CT-ivle-mono2-12bits.dcm", "/tmp/decompressed/single-frame/CT-ivle-mono2-12bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            // passes
            dataList.add( new Data("single-frame/US-evbe-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evbe-rgb-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            // passes
            dataList.add( new Data("single-frame/US-evle-mono2-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-mono2-8bits.dcm", new Rectangle2D.Float(700, 400, 200, 200)));
            // passes
            dataList.add( new Data("single-frame/US-evle-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-rgb-8bits.dcm", new Rectangle2D.Float(100, 10, 50, 50)));
            // fails
            dataList.add( new Data("multi-frame/us-rle-8bit.dcm", "/tmp/decompressed/multi-frame/us-rle-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            // fails
            dataList.add( new Data("multi-frame/US-rle-pal-8bits.dcm", "/tmp/decompressed/multi-frame/US-rle-pal-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            // fails
            dataList.add( new Data("multi-frame/xa-jpeg1.dcm", "/tmp/decompressed/multi-frame/xa-jpeg1.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            // passes
            dataList.add( new Data("multi-frame/us-evle-rgb-8bit.dcm", "/tmp/decompressed/multi-frame/us-evle-rgb-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
//            dataList.add( new Data("IMG1.dcm", "/tmp/decompressed/multi-frame/us-evle-rgb-8bit.dcm", new Rectangle2D.Float(120, 20, 40, 40)));

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( Data data: dataList) {
                File src = new File(loader.getResource( data.inputFileName).toURI());
                File dst = new File( data.outputFileName);
                DicomObjectI dobj = DicomObjectFactory.newInstance( src);
                Rectangle2D rect = (Rectangle2D) data.shape;
                PixelmedPixelEditHandler dbpeh = new PixelmedPixelEditHandler();
                Color c = new Color(128, 128, 128);
                dbpeh.process(rect, c, dobj);
                if( ! dst.getParentFile().exists()) Files.createDirectories( dst.getParentFile().toPath());
                dobj.write( new FileOutputStream( dst));
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    @Test
    @Disabled("Loads DICOM files from local folders")
    void processDarkoData() {
        try {
            List<Data> dataList = new ArrayList<>();
            dataList.add( new Data("darkoData/1.2.840.113654.2.45.2.108105-1-1-2z65o7.dcm", "/tmp/darko/pe.dcm", new Rectangle2D.Float(50, 50, 100, 100)));

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( Data data: dataList) {
                File src = new File(loader.getResource( data.inputFileName).toURI());
                File dst = new File( data.outputFileName);
                DicomObjectI dobj = DicomObjectFactory.newInstance( src);
                Rectangle2D rect = (Rectangle2D) data.shape;
                PixelmedPixelEditHandler dbpeh = new PixelmedPixelEditHandler();
                Color c = new Color(128, 128, 128);
                dbpeh.process(rect, c, dobj);
                if( ! dst.getParentFile().exists()) Files.createDirectories( dst.getParentFile().toPath());
                dobj.write( new FileOutputStream( dst));
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    public class Data {
        String inputFileName;
        String outputFileName;
        Shape shape;
        public Data( String i, String o, Shape s) {
            inputFileName = i;
            outputFileName = o;
            shape = s;
        }
    }
}