package org.nrg.dicom.pixeledit.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Assertions;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.fail;

class DicomImageBlackoutTest {

    /**
     * Images in /tmp/compressed and /tmp/decompressed need to be viewed to judge if regions were altered.
     */
    @Test
    @Disabled
    public void process() {
        try {
            List<DicomImageBlackoutTest.Data> dataList = new ArrayList<>();
            dataList.add( new Data("single-frame/CT-ivle-mono2-12bits.dcm", "/tmp/decompressed/single-frame/CT-ivle-mono2-12bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("single-frame/US-evbe-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evbe-rgb-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("single-frame/US-evle-mono2-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-mono2-8bits.dcm", new Rectangle2D.Float(700, 400, 200, 200)));
            dataList.add( new Data("single-frame/US-evle-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-rgb-8bits.dcm", new Rectangle2D.Float(100, 10, 50, 50)));
            dataList.add( new Data("multi-frame/us-rle-8bit.dcm", "/tmp/compressed/multi-frame/us-rle-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/US-rle-pal-8bits.dcm", "/tmp/compressed/multi-frame/US-rle-pal-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/xa-jpeg1.dcm", "/tmp/compressed/multi-frame/xa-jpeg1.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/us-evle-rgb-8bit.dcm", "/tmp/decompressed/multi-frame/us-evle-rgb-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("single-frame/US-jpg-ybr-8bits.dcm", "/tmp/compressed/single-frame/US-jpg-ybr-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));

            DicomImageBlackout dicomImageBlackout = new DicomImageBlackout();
            Vector shapes = new Vector();

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( DicomImageBlackoutTest.Data data: dataList) {
//                File src = new File(loader.getResource( data.inputFileName).toURI());
//                File dst = new File( data.outputFileName);
                File srcFile = new File(loader.getResource( data.inputFileName).getFile());
                File dstFile = new File( data.outputFileName);
                shapes.clear();
                Rectangle2D rect = (Rectangle2D) data.shape;
                shapes.add( rect);

                dicomImageBlackout.process( srcFile, dstFile, shapes);
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    @Test
    @Disabled
    // not blocking any compressed now.
    public void testTxferSyntaxFilterReject() {
        try {
            List<DicomImageBlackoutTest.Data> dataList = new ArrayList<>();
//            dataList.add( new Data("single-frame/CT-ivle-mono2-12bits.dcm", "/tmp/decompressed/single-frame/CT-ivle-mono2-12bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
//            dataList.add( new Data("single-frame/US-evbe-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evbe-rgb-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
//            dataList.add( new Data("single-frame/US-evle-mono2-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-mono2-8bits.dcm", new Rectangle2D.Float(700, 400, 200, 200)));
//            dataList.add( new Data("single-frame/US-evle-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-rgb-8bits.dcm", new Rectangle2D.Float(100, 10, 50, 50)));
            dataList.add( new Data("multi-frame/us-rle-8bit.dcm", "/tmp/compressed/multi-frame/us-rle-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/US-rle-pal-8bits.dcm", "/tmp/compressed/multi-frame/US-rle-pal-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/xa-jpeg1.dcm", "/tmp/compressed/multi-frame/xa-jpeg1.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
//            dataList.add( new DicomImageBlackoutTest.Data("multi-frame/us-evle-rgb-8bit.dcm", "/tmp/decompressed/multi-frame/us-evle-rgb-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new DicomImageBlackoutTest.Data("single-frame/US-jpg-ybr-8bits.dcm", "/tmp/compressed/single-frame/US-jpg-ybr-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));

            DicomImageBlackout dicomImageBlackout = new DicomImageBlackout();
            Vector shapes = new Vector();

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( DicomImageBlackoutTest.Data data: dataList) {
                File srcFile = new File(loader.getResource( data.inputFileName).getFile());
                File dstFile = new File( data.outputFileName);
                shapes.clear();
                Rectangle2D rect = (Rectangle2D) data.shape;
                shapes.add( rect);

                try {
                    dicomImageBlackout.process(srcFile, dstFile, shapes);
                    fail("Unexpected success.");
                }
                catch( Exception e) {
                    Assertions.assertTrue( e.getMessage().contains("Read failed"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    @Test
    public void testTxferSyntaxFilterAllow() {
        try {
            List<DicomImageBlackoutTest.Data> dataList = new ArrayList<>();
            dataList.add( new Data("single-frame/CT-ivle-mono2-12bits.dcm", "/tmp/decompressed/single-frame/CT-ivle-mono2-12bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("single-frame/US-evbe-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evbe-rgb-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("single-frame/US-evle-mono2-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-mono2-8bits.dcm", new Rectangle2D.Float(700, 400, 200, 200)));
            dataList.add( new Data("single-frame/US-evle-rgb-8bits.dcm", "/tmp/decompressed/single-frame/US-evle-rgb-8bits.dcm", new Rectangle2D.Float(100, 10, 50, 50)));
            dataList.add( new Data("multi-frame/us-rle-8bit.dcm", "/tmp/compressed/multi-frame/us-rle-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/US-rle-pal-8bits.dcm", "/tmp/compressed/multi-frame/US-rle-pal-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new Data("multi-frame/xa-jpeg1.dcm", "/tmp/compressed/multi-frame/xa-jpeg1.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new DicomImageBlackoutTest.Data("multi-frame/us-evle-rgb-8bit.dcm", "/tmp/decompressed/multi-frame/us-evle-rgb-8bit.dcm", new Rectangle2D.Float(200, 200, 100, 100)));
            dataList.add( new DicomImageBlackoutTest.Data("single-frame/US-jpg-ybr-8bits.dcm", "/tmp/compressed/single-frame/US-jpg-ybr-8bits.dcm", new Rectangle2D.Float(200, 200, 100, 100)));

            DicomImageBlackout dicomImageBlackout = new DicomImageBlackout();
            Vector shapes = new Vector();

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( DicomImageBlackoutTest.Data data: dataList) {
                File srcFile = new File(loader.getResource( data.inputFileName).getFile());
                File dstFile = new File( data.outputFileName);
                shapes.clear();
                Rectangle2D rect = (Rectangle2D) data.shape;
                shapes.add( rect);

                dicomImageBlackout.process(srcFile, dstFile, shapes);
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    /**
     * Images in /tmp/compressed and /tmp/decompressed need to be viewed to judge if regions were altered.
     *
     * This data has PHI. Don't put it in the repo.
     */
    @Test
    @Disabled
    public void processCTRACT() {
        try {
            List<DicomImageBlackoutTest.Data> dataList = new ArrayList<>();
            dataList.add( new Data("/Users/drm/Box/CTRACT_Data/Burned_in_PHI/Northwestern_TestCase1_US1/SCANS/1-Venous/DICOM/Northwestern_TestCase1.US.CTRACT.1.16.20180515.093201.125sgoq.dcm", "/tmp/ctract/single/us.dcm", new Rectangle2D.Float(0, 0, 100, 100)));
            dataList.add( new Data( "/Users/drm/Box/CTRACT_Data/Burned_in_PHI/Northwestern_TestCase1_US1/SCANS/1-Venous/DICOM/Northwestern_TestCase1.US.CTRACT.1.8.20180515.093201.ijn36n.dcm", "/tmp/ctract/multi/us_multi.dcm", new Rectangle2D.Float(0, 0, 880, 40)));

            DicomImageBlackout dicomImageBlackout = new DicomImageBlackout();
            Vector shapes = new Vector();

            ClassLoader loader = PixelmedPixelEditHandler.class.getClassLoader();
            for( DicomImageBlackoutTest.Data data: dataList) {
                File srcFile = new File( data.inputFileName);
                File dstFile = new File( data.outputFileName);
                shapes.clear();
                Rectangle2D rect = (Rectangle2D) data.shape;
                shapes.add( rect);

                dicomImageBlackout.process( srcFile, dstFile, shapes);
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    public class Data {
        String inputFileName;
        String outputFileName;
        Shape shape;
        public Data( String i, String o, Shape s) {
            inputFileName = i;
            outputFileName = o;
            shape = s;
        }
    }
}